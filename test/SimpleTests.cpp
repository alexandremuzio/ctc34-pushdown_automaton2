//
// Created by muzio on 05/11/15.
//

#include "gtest/gtest.h"
#include "FileReader.h"

TEST(FileReader, test_eq) {
    FileReader f("/home/muzio/ClionProjects/pushdown_automaton/test/automaton1.txt");
    f.read();

    std::string fileRead;
    fileRead = f.getContent();
    EXPECT_EQ("4 7\n1 2 e e $\n2 2 a e a\n2 3 e e e\n3 3 b a e\n3 4 e $ e\n4 4 c e e\n2 5 e e e\n5 5 b e e\n5 6 e e e\n6 6 c a e\n6 7 e $ e\n",
              fileRead);
}

TEST(basic_check, test_neq) {
    EXPECT_NE(1, 0);
}
