//
// Created by muzio on 05/11/15.
//

#include "gtest/gtest.h"
#include "PushdownAutomatonGenerator.h"
#include "FileReader.h"
#include "AutomatonSimulator.h"

TEST(AutomatonSimulator, Simulation1) {
    FileReader f("/home/muzio/ClionProjects/pushdown_automaton/test/automaton1.txt");
    f.read();

    PushdownAutomatonGenerator generator(f.getContent());
    PushdownAutomaton p = generator.generate();

    AutomatonSimulator sim(p, "aabcc");
    EXPECT_EQ(true, sim.run());
}

TEST(AutomatonSimulator, Simulation2) {
    FileReader f("/home/muzio/ClionProjects/pushdown_automaton/test/automaton2.txt");
    f.read();

    PushdownAutomatonGenerator generator(f.getContent());
    PushdownAutomaton p = generator.generate();

    AutomatonSimulator sim(p, "000111");
    EXPECT_EQ(true, sim.run());
}


TEST(AutomatonSimulator, Simulation3) {
    FileReader f("/home/muzio/ClionProjects/pushdown_automaton/test/automaton3.txt");
    f.read();

    PushdownAutomatonGenerator generator(f.getContent());
    PushdownAutomaton p = generator.generate();

    AutomatonSimulator sim(p, "aba");
    EXPECT_EQ(true, sim.run());
}

