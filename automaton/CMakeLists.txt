cmake_minimum_required(VERSION 3.3)
project(pushdown_automaton)

set(HEADER_FILES Digraph.h Edge.h FileReader.h PushdownAutomaton.h AutomatonSimulator.h StackInstruction.h PushdownAutomatonGenerator.h Utils.h)

set(SOURCE_FILES FileReader.cpp PushdownAutomaton.cpp PushdownAutomatonGenerator.cpp StackInstruction.cpp AutomatonSimulator.cpp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_library(pushdown_automaton STATIC ${SOURCE_FILES} ${HEADER_FILES})