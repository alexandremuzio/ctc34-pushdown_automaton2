//
// Created by muzio on 05/11/15.
//

#include "AutomatonSimulator.h"

#include <iostream>

#include "Utils.h"

AutomatonSimulator::AutomatonSimulator(const PushdownAutomaton &p, std::string chain) : automaton_(p), chain_(chain){
}

bool AutomatonSimulator::run() {
    return recursiveDfs(std::stack<char>(), 1, chain_);
}

bool AutomatonSimulator::recursiveDfs(const std::stack<char> &s, int orig,
                                      std::string chain) {
    printPossibleStates(s, orig, chain);
    if (chain.empty() && automaton_.isAcceptState(orig)) return true;
    //if (chain.empty() && !automaton_.isAcceptState(orig)) return false;

    bool foundAcceptState = false;
    for (auto &edge : automaton_.neighborStates(orig)) {

        //do stack operations
        char read = edge.val().getReadPop();
        char wrote = edge.val().getWritePush();
        std::stack<char> copiedStack(s);

        char readFromStack = 0;
        if (read != 'e' && !s.empty()) {
            readFromStack = copiedStack.top();
            copiedStack.pop();
        }
        else if (read != 'e' && s.empty()) {
            continue;
        }

        if (wrote != 'e') {
            copiedStack.push(wrote);
        }

        if (read == 'e' || readFromStack == read) {
            if (edge.val().getVal() == 'e')
                foundAcceptState |= recursiveDfs(copiedStack, edge.dest(), chain);
            else if (edge.val().getVal() == chain[0])
                foundAcceptState |= recursiveDfs(copiedStack, edge.dest(), chain.substr(1));
        }

        //condicao de saida antecipada
        if (foundAcceptState) {
            return true;
        }
    }

    return foundAcceptState;
}

void AutomatonSimulator::printPossibleStates(const std::stack<char> &s, int orig,
                                             std::string chain) {
    std::cout << "---------------------" << std::endl;
    std::cout << "Estado: q" << orig << std::endl;
    if (chain.empty()  && automaton_.isAcceptState(orig)){
        std::cout << "Estado aceito!" << std::endl;
        return;
    }

    std::cout << "Pilha: " << Utils::printStack(s) << std::endl;
    std::cout << "Cadeia: " << chain << std::endl << std::endl;
    std::cout << "Estados possiveis: ";
    for (auto &edge : automaton_.neighborStates(orig)) {
        char read = edge.val().getReadPop();

        char readFromStack = 0;
        if (read != 'e' && !s.empty()) {
            readFromStack = s.top();
        }
        else if (read != 'e' && s.empty()) {
            continue;
        }

        if (read == 'e' || readFromStack == read) {
            if (edge.val().getVal() == 'e' ||
                    edge.val().getVal() == chain[0]) {
                std::cout << "q" + std::to_string(edge.dest()) << " ";
            }
        }
    }
    std::cout << std::endl;
}
