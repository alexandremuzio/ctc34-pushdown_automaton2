//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_FILEREADER_H
#define PUSHDOWN_AUTOMATON_FILEREADER_H

#include <string>

class FileReader {
public:
    FileReader(std::string filename);
    ~FileReader();
    void read();
    std::string& getContent();

private:
    std::string _filename;
    std::string _fileContent;
};



#endif //PUSHDOWN_AUTOMATON_FILEREADER_H
