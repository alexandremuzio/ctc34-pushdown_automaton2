//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_EDGE_H
#define PUSHDOWN_AUTOMATON_EDGE_H

template <class T>
class Edge {
public:
    Edge(int orig, int dest, T val) : val_(val){
        orig_ = orig;
        dest_ = dest;
    }

    virtual ~Edge() { }

    int orig() const {
        return orig_;
    }

    int dest() const {
        return dest_;
    }

    T val() const {
        return val_;
    }

private:
    int orig_;
    int dest_;
    T val_;
};


#endif //PUSHDOWN_AUTOMATON_EDGE_H
