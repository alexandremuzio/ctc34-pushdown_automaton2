//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_AUTOMATONSIMULATOR_H
#define PUSHDOWN_AUTOMATON_AUTOMATONSIMULATOR_H


#include "PushdownAutomaton.h"

class AutomatonSimulator {
public:
    AutomatonSimulator(const PushdownAutomaton &p, std::string chain);


    bool run();

private:
    bool recursiveDfs(const std::stack<char> &s, int orig, std::string chain);
    void printPossibleStates(const std::stack<char> &s, int orig,
                             std::string chain);

    const PushdownAutomaton & automaton_;
    const std::string chain_;
};


#endif //PUSHDOWN_AUTOMATON_AUTOMATONSIMULATOR_H
