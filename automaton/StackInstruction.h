//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_STACKINSTRUCTION_H
#define PUSHDOWN_AUTOMATON_STACKINSTRUCTION_H

#include <string>

class StackInstruction {
public:
    StackInstruction(char val, char readPop, char writePush);

    char getVal();
    char getReadPop();
    char getWritePush();

private:
    char val_;
    char readPop_;
    char writePush_;
};


#endif //PUSHDOWN_AUTOMATON_STACKINSTRUCTION_H
