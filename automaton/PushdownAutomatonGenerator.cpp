//
// Created by muzio on 05/11/15.
//

#include "PushdownAutomatonGenerator.h"

#include <sstream>
#include <iostream>

PushdownAutomatonGenerator::PushdownAutomatonGenerator(std::string automatonString) {
    automatonString_ = automatonString;
}

PushdownAutomatonGenerator::~PushdownAutomatonGenerator() {
}

PushdownAutomaton PushdownAutomatonGenerator::generate() {
    std::istringstream f(automatonString_);

    PushdownAutomaton pAutomaton;

    std::string line, acceptState;
    std::getline(f, line);
    std::istringstream issT(line);
    while(issT >> acceptState) {
        //std::cout << acceptState << std::endl;
        pAutomaton.addAcceptState(std::stoi(acceptState));
    }

    while (std::getline(f, line)) {
        std::istringstream iss(line);
        std::string orig, dest, val, instra, instrb;

        iss >> orig;
        iss >> dest;
        iss >> val;
        iss >> instra;
        iss >> instrb;

        Edge<StackInstruction> newEdge = Edge<StackInstruction>(std::stoi(orig), std::stoi(dest), StackInstruction(val[0], instra[0], instrb[0]));
        pAutomaton.addStateEdge(
                Edge<StackInstruction>(std::stoi(orig), std::stoi(dest), StackInstruction(val[0], instra[0], instrb[0])));

        //std::cout << val[0] << " " << instra[0] << " " << instrb[0] << std::endl;
    }

    return pAutomaton;
}
