//
// Created by muzio on 05/11/15.
//

#include "PushdownAutomaton.h"

#include <algorithm>

PushdownAutomaton::PushdownAutomaton(int maxStates) :states_(maxStates) {
}

PushdownAutomaton::~PushdownAutomaton() {

}

void PushdownAutomaton::addStateEdge(Edge<StackInstruction> edge) {
    states_.addEdge(edge.orig(), edge);
}

void PushdownAutomaton::addAcceptState(int state) {
    acceptStates_.push_back(state);
}

const std::vector<Edge<StackInstruction>> &PushdownAutomaton::neighborStates(int ori) const {
    return states_.neighbors(ori);
}

bool PushdownAutomaton::stackIsEmpty() const {
    return stack_.empty();
}

bool PushdownAutomaton::isAcceptState(int orig) const {
    return std::find(acceptStates_.begin(), acceptStates_.end(), orig) != acceptStates_.end();
}
