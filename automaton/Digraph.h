//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_DIGRAPH_H
#define PUSHDOWN_AUTOMATON_DIGRAPH_H


#include <vector>

template<class T>
class Digraph {
public:
    Digraph(int size) {
        _v = size;
        _e = 0;
        _gr = new std::vector<T>[size + 1];
    }

    ~Digraph() { }

    void addEdge(int orig, T edge) {
        _e++;
        _gr[orig].push_back(edge);
    }

    const std::vector<T> & neighbors(int i) const {
        return _gr[i];
    }

    int V() const {
        return _v;
    }

    int E() const {
        return _e;
    }

private:
    std::vector<T> *  _gr;
    int _v;
    int _e;
};

#endif //PUSHDOWN_AUTOMATON_DIGRAPH_H
