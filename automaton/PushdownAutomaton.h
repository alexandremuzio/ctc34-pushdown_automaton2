//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATON_H
#define PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATON_H

#include <stack>

#include "Digraph.h"
#include "Edge.h"
#include "StackInstruction.h"

class PushdownAutomaton {
public:
    PushdownAutomaton(int nMaxStates = 10);
    virtual ~PushdownAutomaton();

    void addStateEdge(Edge<StackInstruction> edge);
    void addAcceptState(int state);

    const std::vector<Edge<StackInstruction>> &neighborStates(int ori) const;
    bool stackIsEmpty() const;
    bool isAcceptState(int orig) const;

private:
    Digraph<Edge<StackInstruction>> states_;
    std::stack<char> stack_;
    std::vector<int> acceptStates_;
    std::string chain_;
};


#endif //PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATON_H
