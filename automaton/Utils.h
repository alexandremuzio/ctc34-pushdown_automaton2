//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_UTILS_H
#define PUSHDOWN_AUTOMATON_UTILS_H

#include <stack>
#include <string>

class Utils {
public :
    static std::string printStack(std::stack<char> s) {
        std::stack<char> helperS;
        std::string retString;

        while (!s.empty()) {
            helperS.push(s.top());
            s.pop();
        }

        while (!helperS.empty()) {
            retString +=  helperS.top();
            helperS.pop();
        }
        return retString;
    }
};


#endif //PUSHDOWN_AUTOMATON_UTILS_H
