//
// Created by muzio on 05/11/15.
//

#include "StackInstruction.h"
#include <iostream>

StackInstruction::StackInstruction(char val, char readPop, char writePush) {
    val_ = val;
    readPop_ = readPop;
    writePush_ = writePush;
}

char StackInstruction::getVal() {
    return val_;
}

char StackInstruction::getReadPop() {
    return readPop_;
}

char StackInstruction::getWritePush() {
    return writePush_;
}
