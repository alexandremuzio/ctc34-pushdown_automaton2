//
// Created by muzio on 05/11/15.
//

#include "FileReader.h"

#include <iostream>
#include <fstream>
#include <sstream>

FileReader::FileReader(std::string filename) {
    _filename = filename;
}

FileReader::~FileReader() {
}

void FileReader::read() {
    std::string line;
    std::ifstream myfile (_filename);

    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            _fileContent.append(line);
            _fileContent.append("\n");
        }

        myfile.close();
    }

    else
        std::cout << "Unable to open file!" << std::endl;
}

std::string& FileReader::getContent() {
    return _fileContent;
}
