//
// Created by muzio on 05/11/15.
//

#ifndef PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATONGENERATOR_H
#define PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATONGENERATOR_H


#include <string>
#include "PushdownAutomaton.h"

class PushdownAutomatonGenerator {
public:
    PushdownAutomatonGenerator(std::string automatonString);
    virtual  ~PushdownAutomatonGenerator();

    PushdownAutomaton generate();
private:
    std::string automatonString_;
};


#endif //PUSHDOWN_AUTOMATON_PUSHDOWNAUTOMATONGENERATOR_H
